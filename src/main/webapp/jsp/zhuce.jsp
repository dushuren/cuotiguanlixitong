<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>注册页面</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/index.css"/>
		<script src="js/jquery-2.1.1.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<!--注册页面-->
		<div class="fengmian">
			<!--登录页面的背景-->
			<div class="fengmian-bg" id="denglu-bg">
				<!--使用栅格系统,将整个登录框放在中间，左3中6右3-->
				<div class="row" id="fengmian-row" >
					<!--登录框--> <!--向右偏移3，中6-->
					<div class="denglukuang col-lg-offset-3 col-lg-6 ">
						<!--标题-->
						<h1 class="text-center" style="margin-bottom: 40px;">
							<a href="denglu" id="denglu-wangji">欢迎注册错题管理系统</a>
							<br/><small style="color: #bb0000;">${requestScope.message}</small>
						</h1>
						<!--里面的表单   这里用水平的-->
						<form:form   role="form" class="form-horizontal" action="/ZhuceUser"
						  modelAttribute="s" method="post">
							<!--一行里面的内容-->
							<div class="form-group">
								<!--必须有label-->
								<label class="col-lg-2 control-label">用户名:</label>
								<!--这里必须用一个div来框起来-->
								<div class="col-lg-10">
									<form:input path="name" placeholder="UserName" cssClass="form-control" maxlength="10"/>
									<form:errors path="name" cssClass="span-error"></form:errors>
								</div>
							</div>
							<!--密码-->
							<div class="form-group">
								<label class="col-lg-2 control-label">密码:</label>
								<div class="col-lg-10">
								    <input type="password" name="password" placeholder="Password" class="form-control" maxlength="12" value="${password2}"/>
									<%-- <form:password path="password"  placeholder="Password"  cssClass="form-control" maxlength="12"/> --%>
									<form:errors path="password" cssClass="span-error"></form:errors>
								</div>
							</div>
							<!--Emai-->
							<div class="form-group">
								<!--email-->
								<label class="col-lg-2 control-label">邮箱:</label>
								<!--输入框，占10个-->
								<div class="col-lg-10">
									<form:input path="email"  placeholder="Email" cssClass="form-control"/>
									<form:errors path="email" cssClass="span-error"></form:errors>
								</div>
							</div>
							<!--邮箱验证码-->
							<div class="form-group">
								<!--验证码-->
								<label class="col-lg-2 control-label">验证码:</label>
								<!--输入框，占8个-->
								<div class="col-lg-8">
									<input name="yanzhengma" type="text" placeholder="Verification Code" class="form-control" value="${yanzhengma }"  />
									<span class="span-error" >${yanzhengma2}</span>
								</div>
								<!--一张验证码图片-->
								<div class="col-lg-2">
									<img class="yanzhengma img-rounded"  src="/Yanzhengma" id="yanzhengma"  onclick="changeCode()"/>
								</div>
							</div>
							<!--底下的登录按钮-->
							<div class="form-group">
								<!--左偏移2，占10-->
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-lg btn-info denglu-btn">立即注册</button>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
		
		<!--底部-->
		<div class="dibu">
			<!--底部的内容-->
			<div class="well well-lg text-center " id="dibu-text">
				<h3><small>Copyright © 2019 德玛西亚 
					 <a id="mytooltip" href="http://www.zhaoxingyu.xyz" data-toggle="tooltip" title=""
						data-placement="bottom" data-original-title="http://www.zhaoxingyu.xyz"
						data-animation="false">
					 	zhaoxingyu.xyz
					</a>
					  All Rights Reserved. 备案号：蜀ICP备18022548号</small></h3>
			</div>
		</div>
		<script type="text/javascript">
			$('[data-toggle="tooltip"]').tooltip();//移动到底部那就显示
			
			//这里是 不断刷新验证码
			function changeCode() {
				//从文档中取出img图片
				var img = document.getElementById("yanzhengma");
				//获取图片的src 属性 并赋值
				//注意:如果请求网址完全相同 则浏览器不会帮你刷新
				//可以拼接当前时间  让每次请求的网址都不一样

				img.src = "/Yanzhengma?time " + new Date().getTime();
			}

		</script>
	
	</body>
</html>
