<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>错题管理专业课主页</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/index.css"/>
		<script src="js/jquery-2.1.1.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body  data-spy="scroll" data-target="#myScrollspy">
		<!--上面放个导航条-->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<!--容器-->
			<div class="container">
				<!--想用栅格系统-->
				<div class="row">
					
					<div class="col-lg-2">
						<!--具体内容，导航条的开头,也就是最开始的文字-->
						<div class="navbar-left navbar-header">
							<!--brand 品牌-->
							<a href="index" class="navbar-brand"><%=session.getAttribute("username")%></a>
							<!--加上一个折叠的按钮-->      <!--toggle：切换-->
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#zhedie">
								<!--切换导航，相当于3个span弄成一个图标按钮-->
								<span class="sr-only">切换导航</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
					</div>
					
					<div class="col-lg-7 col-lg-offset-3">
						<!--折叠的框 ，也是导航条里的主要内容-->
						<div class="collapse navbar-collapse navbar-left" id="zhedie">
							<!--导航内容-->
							<ul class="nav navbar-nav " id="mytab">
								<li class=" dropdown">
									<a href="zhuye-shuxue" class="dropdown-toggle " data-toggle="dropdown">  
										数学<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye-shuxue">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class=" dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										英语<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye-yingyu">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class=" dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										政治<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye-zhengzhi">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class="active dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										专业课<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye-zhuanyeke">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
							</ul>
							
							<!--搜索框-->
							<form class="navbar-form navbar-left " role="search">
								<!--form的内容部分-->
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search..." />
								</div>
								<button type="submit" class="btn btn-info">搜索</button>
							</form>
						</div>
					</div>
					
					
				</div>
				

			</div>
		</div>
		
		<!--注册页面-->
		<div class="fengmian ">
			<!--数学主页的背景-->
			<div class="fengmian-bg" id="shuxue-zhuye-bg">
				<!--这里才是，这回写的，中间左边放日期   右边放面板-->
				<div class="zhongjian">
					<div class="container">
						<!--用栅格系统-->
						<div class="row">
							<!--左边的日期-->
							<div class="col-lg-2 " id="myScrollspy">
								<!--这里放 列表-->
								<ul class="nav nav-tabs nav-stacked "   data-spy="affix" data-offset-top="-50">
									<c:forEach items="${requestScope.clist}" var="clist">
									<li>
										<!--item-->
										<a href="#${clist.id}">
											<h4 >${clist.date}</h4>
										</a>
									</li>
									</c:forEach>
									
									
									
								</ul>
						
							</div>
							
							
							<!--右边的面板-->
							<div class="col-lg-10" >
							        <c:forEach items="${requestScope.clist}" var="clist">
									<!--面板-->
									<div class="panel panel-info" id="${clist.id}" >
										<!--头部-->
										<div class="panel-heading">
											<h4>${clist.theme}</h4>
										</div>
										<!--身体部分-->
										<div class="panel-body">
											<!--左边两张图片，然后具体内容-->
											<div class="col-lg-4">
												<img src="upload/${clist.image_timu}"  class="zhuye-tupian" />
											</div>
											<div class="col-lg-4">
												<img src="upload/${clist.image_daan}" class="zhuye-tupian" />
											</div>
											<div class="col-lg-4">
												${clist.content}
												<br /><br />
												
											</div>
										</div>
										<!--底部-->
										<div class="panel-footer text-right">
											${clist.date}
										</div>
		
									</div>
									</c:forEach>
									
									
								</div>
						
							<!--底部-->
							<div class="dibu">
								<!--右移动2 共10-->
									<div class="col-lg-offset-2 col-lg-10">
										<!--底部的内容-->
										<div class="well well-lg text-center " id="dibu-text">
											<h3><small>Copyright © 2019 德玛西亚 
												 <a id="mytooltip" href="http://www.zhaoxingyu.xyz" data-toggle="tooltip" title=""
													data-placement="bottom" data-original-title="http://www.zhaoxingyu.xyz"
													data-animation="false">
												 	zhaoxingyu.xyz
												</a>
												  All Rights Reserved. 备案号：蜀ICP备18022548号</small></h3>
										</div>
									</div>
							</div>
					
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
		
		
			
		</div>
		<script type="text/javascript">
			$('[data-toggle="tooltip"]').tooltip();//移动到底部那就显示
		</script>
		
	</body>
</html>
