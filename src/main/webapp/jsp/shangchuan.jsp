<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>错题管理上传页面</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/index.css"/>
		<script src="js/jquery-2.1.1.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/shangchuan.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body  data-spy="scroll" data-target="#myScrollspy">
		<!--上面放个导航条-->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<!--容器-->
			<div class="container">
				<!--想用栅格系统-->
				<div class="row">
					
					<div class="col-lg-2">
						<!--具体内容，导航条的开头,也就是最开始的文字-->
						<div class="navbar-left navbar-header">
							<!--brand 品牌-->
							<a href="index" class="navbar-brand"><%=session.getAttribute("username")%></a>
							<!--加上一个折叠的按钮-->      <!--toggle：切换-->
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#zhedie">
								<!--切换导航，相当于3个span弄成一个图标按钮-->
								<span class="sr-only">切换导航</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
					</div>
					
					<div class="col-lg-7 col-lg-offset-3">
						<!--折叠的框 ，也是导航条里的主要内容-->
						<div class="collapse navbar-collapse navbar-left" id="zhedie">
							<!--导航内容-->
							<ul class="nav navbar-nav " id="mytab">
								<li class=" dropdown">
									<a href="zhuye-shuxue" class="dropdown-toggle " data-toggle="dropdown">  
										数学<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye-shuxue">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class=" dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										英语<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye-yingyu">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class=" dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										政治<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye-zhengzhi">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class=" dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										专业课<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye-zhuanyeke">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
							</ul>
							
							<!--搜索框-->
							<form class="navbar-form navbar-left " role="search">
								<!--form的内容部分-->
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search..." />
								</div>
								<button type="submit" class="btn btn-info">搜索</button>
							</form>
						</div>
					</div>
					
					
				</div>
				

			</div>
		</div>
		
			<!--容器-->
			<div class="container">
				<!--想用栅格系统-->
				<div class="row">
					
					<div class="col-lg-2">
						<!--具体内容，导航条的开头,也就是最开始的文字-->
						<div class="navbar-left navbar-header">
							<!--brand 品牌-->
							<a href="index" class="navbar-brand"><%=session.getAttribute("username")%></a>
							<!--加上一个折叠的按钮-->      <!--toggle：切换-->
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#zhedie">
								<!--切换导航，相当于3个span弄成一个图标按钮-->
								<span class="sr-only">切换导航</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
					</div>
					
					<div class="col-lg-7 col-lg-offset-3">
						<!--折叠的框 ，也是导航条里的主要内容-->
						<div class="collapse navbar-collapse navbar-left" id="zhedie">
							<!--导航内容-->
							<ul class="nav navbar-nav " id="mytab">
								<li class=" dropdown">
									<a href="zhuye-shuxue" class="dropdown-toggle " data-toggle="dropdown">
										数学<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class=" dropdown">
									<a href="zhuye-yingyu" class="dropdown-toggle" data-toggle="dropdown">
										英语<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class=" dropdown">
									<a href="zhuye-zhengzhi" class="dropdown-toggle" data-toggle="dropdown">
										政治<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
								<li class=" dropdown">
									<a href="zhuye-zhuanyeke" class="dropdown-toggle" data-toggle="dropdown">
										专业课<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="zhuye">学习记录</a></li>
										<li><a href="shangchuan">上传记录</a></li>
									</ul>
								</li>
							</ul>
							
							<!--搜索框-->
							<form class="navbar-form navbar-left " role="search">
								<!--form的内容部分-->
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search..." />
								</div>
								<button type="submit" class="btn btn-info">搜索</button>
							</form>
						</div>
					</div>
					
					
				</div>
				

			</div>
		</div>
		
		<!--上传文件的地方-->
		<!--注册页面-->
		<div class="fengmian">
			<!--登录页面的背景-->
			<div class="fengmian-bg" id="shuxue-shangchuan-bg">
				<!--使用栅格系统,将整个登录框放在中间，左3中6右3-->
				<div class="row" id="fengmian-row" >
					<!--登录框--> <!--向右偏移3，中6-->
					<div class="denglukuang col-lg-offset-3 col-lg-6 "
						style="margin-top: 2%;">
						<!--标题-->
						<h1 class="text-center" style="margin-bottom: 40px;">
							<a href="shuxue_shangchuan" id="denglu-wangji">上传学习记录</a>
						</h1>
						<!--里面的表单   这里用水平的-->
						<form  role="form" class="form-horizontal" action="/ShangchuanStudy" 
						method="post" enctype="multipart/form-data">
							<!--选择科目-->
							<div class="form-group">
								<!--必须有label-->
								<label class="col-lg-2 control-label">上传的科目:</label>
								<!--这里必须用一个div来框起来-->
								<div class="col-lg-10">
									<select class="form-control" name="subject">
										<option selected="selected">数学</option>
										<option >英语</option>
										<option>政治</option>
										<option>专业课</option>
									</select>
								</div>
							</div>
							<!--一行里面的内容-->
							<div class="form-group">
								<!--必须有label-->
								<label class="col-lg-2 control-label">主题:</label>
								<!--这里必须用一个div来框起来-->
								<div class="col-lg-10">
									<input name="theme" type="text" placeholder="The theme" class="form-control" />
								</div>
							</div>
							<!--详细内容-->
							<div class="form-group">
								<label class="col-lg-2 control-label">详细内容:</label>
								<div class="col-lg-10">
									<textarea name="content"  placeholder="The detailed content" class="form-control" 
										rows="5"/></textarea>
								</div>
							</div>
							<!--题目图片-->
							<div class="form-group">
								<!--email-->
								<label class="col-lg-2 control-label">题目图片:</label>
								<!--输入框，占10个-->
								<div class="col-lg-10">
									<input name="file1" type="file" placeholder="Subject images" class="form-control" 
										id="shangchuan" onchange="javascript:setImagePreview();"/>
									<p style="color: #262626;">也可以选择自己认为比较重要的图片哟</p>
								</div>
							</div>
							<!--答案图片-->
							<div class="form-group">
								<!--答案图片-->
								<label class="col-lg-2 control-label">答案图片:</label>
								<!--输入框，占10个-->
								<div class="col-lg-10">
									<input name="file2" type="file" placeholder="Answer images" class="form-control" 
										id="shangchuan2" onchange="javascript:setImagePreview2();" />
									<p style="color: #262626;">也可以选择自己认为比较重要的图片哟</p>
								</div>
							</div>
							<!--底下两张图片-->
							
							<!--这里放两张图片的展示图-->
							<div class="form-group" >
								<!--右移动2，左5，右5-->
								<div class="col-lg-offset-2 col-lg-5"  id="divimg">
									<img id="shangchuanimg" src="img/touxiang/touxiang1.jpg" class="shuxue_shangchuan_img" />
								</div>
								
								<!--左边6，右边6-->
								<div class="col-lg-5"  id="divimg2">
									<img id="shangchuanimg2" src="img/touxiang/touxiang1.jpg" class="shuxue_shangchuan_img" />
								</div>
							</div>
							
							<!--底下的登录按钮-->
							<div class="form-group" >
								<!--左偏移2，占10-->
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-lg btn-info denglu-btn">立即上传</button>
								</div>
							</div>
						</form>
					</div>
				
					<!--底部-->
					<div class="dibu">
						<!--右移动2 共10-->
							<div class="col-lg-12">
								<!--底部的内容-->
								<div class="well well-lg text-center " id="dibu-text">
									<h3><small>Copyright © 2019 德玛西亚 
										 <a id="mytooltip" href="http://www.zhaoxingyu.xyz" data-toggle="tooltip" title=""
											data-placement="bottom" data-original-title="http://www.zhaoxingyu.xyz"
											data-animation="false">
										 	zhaoxingyu.xyz
										</a>
										  All Rights Reserved. 备案号：蜀ICP备18022548号</small></h3>
								</div>
							</div>
					</div>
					
				</div>
			</div>
		</div>
		
		
		<script type="text/javascript">
			$('[data-toggle="tooltip"]').tooltip();//移动到底部那就显示
		</script>
		
	</body>
</html>
