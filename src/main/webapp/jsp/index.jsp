<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>错题管理系统</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/index.css"/>
		<script src="js/jquery-2.1.1.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	</head>
		<body>
		<!--封面-->
		<div class="fengmian" >
			<!--背景-->
			<div class="fengmian-bg" id="fengmianbg">
				<!--使用超大屏幕-->
				<div class="jumbotron " id="fengmian-mianban">
					<!--里面的内容-->
					<div class="container text-center  fengmian-headfont" >
						<h1>欢迎来到错题管理系统</h1>
						<p>在这里你可以管理你的数学、英语、政治、专业课的错题</p>
						<p>
							<a href="zhuce" class="btn btn-info btn-lg">立即注册</a>
							<a href="denglu" class="btn btn-info btn-lg">用户登录</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<!--底部-->
		<div class="dibu">
			<!--底部的内容-->
			<div class="well well-lg text-center " id="dibu-text">
				<h3><small>Copyright © 2019 德玛西亚 
					 <a id="mytooltip" href="http://www.zhaoxingyu.xyz" data-toggle="tooltip" title=""
						data-placement="bottom" data-original-title="http://www.zhaoxingyu.xyz"
						data-animation="false">
					 	zhaoxingyu.xyz
					</a>
					  All Rights Reserved. 备案号：蜀ICP备18022548号</small></h3>
			</div>
		</div>
		<script type="text/javascript">
			$('[data-toggle="tooltip"]').tooltip();//移动到底部那就显示
		</script>
	</body>
</html>
