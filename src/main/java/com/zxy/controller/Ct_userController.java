package com.zxy.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zxy.entity.Ct_user;
import com.zxy.service.Ct_userService;

import cn.dsna.util.images.ValidateCode;
/**
 * 
 * @author zxy
 * @time 2019年8月13日18:30:56
 * @method 有关Ct_user表的控制器
 */
@Controller
public class Ct_userController {
	@Autowired
	private Ct_userService ct_userService;
	//日志 使用log日志，导入包org.slf4g
	private Logger logger=LoggerFactory.getLogger(this.getClass());
	
	
	@RequestMapping(value = "/Yanzhengma")
	public void Yanzhengma(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// 这里专门用了一个请求来生产验证码 长度，宽度，字符个数，下划线数
		// 页面上调用为 src="/项目名/Yanzhengma"
		// 我的原文件里是长90px,宽31px.
		ValidateCode vCode = new ValidateCode(90, 31, 4, 9);
		// 获取生产的验证码字符串
		String code = vCode.getCode();
		// 传值方式1.拼接网址字符串 2.使用域对象
		
		// 使用session 来储存验证码
		request.getSession().setAttribute("code", code);
		// 得到session里的东西 session.getAttribute("code");
		logger.info(code);
		//System.out.println(code);

		// 写到网页上 (通过 响应中的字符流 写回网页)
		vCode.write(response.getOutputStream());
		response.getOutputStream().close();
	    //vCode.write("H:/jepluse/place/XueXiaoSheTuan/WebContent/img/yanzhengma.png");
	    //服务器上的
	    //vCode.write("/home/pan/tomcat/webapps/XueXiaoSheTuan/img/yanzhengma.png");
	}
	
	/*
	 * @method zhuce.jsp里，不加@ModelAttribute它找不到验证哪个实体类，就连页面都显示不出来，而且这里的名字尽量不要有_
	 * 最好就一个字母，要不然也容易出错
	 */
	@RequestMapping("/zhuce")
	public String Zhuce( @ModelAttribute("s") Ct_user ct_user2)
	{
		return "zhuce";
	}
	
	
	/*
	 * @method zhuce.jsp里注册用户
	 */
	@RequestMapping("/ZhuceUser")
	public ModelAndView ZhuceUser(ModelAndView mv,HttpSession session,
			@Valid @ModelAttribute("s") Ct_user ct_user2,Errors errors,
			String name,String password,String email,String yanzhengma)throws Exception
	{
		logger.info("传过来的值为："+name+","+password+","+email+","+yanzhengma);
		logger.info("传过来的ct_user2："+ct_user2.toString());
		
		if(errors.hasErrors())
		{
			logger.info("数据绑定失败");
			mv.addObject("password2", password);
			mv.addObject("yanzhengma",yanzhengma);
			mv.setViewName("zhuce");
			return mv;
		}
		//生成id_user
		SimpleDateFormat dFormat=new SimpleDateFormat("yyyy-MM-dd");
		String now=dFormat.format(new Date());
		String id_user=now+UUID.randomUUID();
		
		//判断验证码是否为空
		if(StringUtils.isEmpty(yanzhengma))
		{
			mv.addObject("yanzhengma2", "验证码不能为空");
			mv.addObject("password2", password);
			mv.addObject("yanzhengma",yanzhengma);
			mv.setViewName("zhuce");
			return mv;
		}
		
		//判断验证码是否正确
		String code=(String)session.getAttribute("code");
		if(!StringUtils.isEmpty(yanzhengma)&&!code.equalsIgnoreCase(yanzhengma))
		{
			mv.addObject("password2", password);
			mv.addObject("yanzhengma",yanzhengma);
			mv.addObject("yanzhengma2", "验证码不正确");
			mv.setViewName("zhuce");
			return mv;
		}
		//判断用户是否已经存在
		Ct_user ct_user3=ct_userService.selectCt_userByname(name);
		if(ct_user3==null)
		{
			//执行插入方法
			Ct_user ct_user=new Ct_user();
			ct_user.setEmail(email);
			ct_user.setId_user(id_user);
			ct_user.setName(name);
			ct_user.setPassword(password);
			ct_userService.insertCt_user(ct_user);
			
			mv.addObject("password2", password);
			mv.addObject("yanzhengma",yanzhengma);
			mv.addObject("message", "恭喜您，注册成功");
			mv.setViewName("zhuce");
			return mv;
		}else {
			//用户已经存在
			mv.addObject("password2", password);
			mv.addObject("yanzhengma",yanzhengma);
			mv.addObject("message", "您已经注册过了或者用户名已经被占用！");
			mv.setViewName("zhuce");
			return mv;
		}
		
		
	}

	/*
	 * @method denglu.jsp里用户登录
	 */
	@RequestMapping(value="/DengluUser",method=RequestMethod.POST)
	public ModelAndView DengluUser(ModelAndView mv,HttpSession session,
			String name,String password,String yanzhengma)
	{
		logger.info("DengluUser里:"+name+","+password+","+yanzhengma);
		
		Boolean ct_userBoolean=false;
		Boolean yanzhengmaBoolean=true;
		
		//name和password不为空
		if(!StringUtils.isEmpty(name)&&!StringUtils.isEmpty(password))
		{
			//先根据用户名和密码查看是否存在
			Ct_user ct_user=new Ct_user();
			ct_user=ct_userService.selectUserByNameAndPassword(name, password);
			//用户存在
			if(ct_user!=null){
				ct_userBoolean=true;
				session.setAttribute("ct_user", ct_user);
				session.setAttribute("username", ct_user.getName());
			}
			
		}
		else 
		{
			logger.info("name或者password为空");
			mv.addObject("message", "用户名或者密码为空");
			
			//将用户输入的值返回去，以免重新输入
			mv.addObject("name2", name);
			mv.addObject("password2",password);
			mv.addObject("yanzhengma2", yanzhengma);
			
			mv.setViewName("denglu");
			return mv;
		}
		
		//判断验证码是否为空
		if(StringUtils.isEmpty(yanzhengma))
		{
			mv.addObject("message", "验证码不能为空");
			//将用户输入的值返回去，以免重新输入
			mv.addObject("name2", name);
			mv.addObject("password2",password);
			mv.addObject("yanzhengma2", yanzhengma);
			mv.setViewName("denglu");
			return mv;
		}
		
		//判断验证码 ,不对有提示
		String code =(String) session.getAttribute("code");
		if(!code.equalsIgnoreCase(yanzhengma)){
			logger.info("验证码错误");
			yanzhengmaBoolean=false;
			mv.addObject("message", "验证码错误");
			//将用户输入的值返回去，以免重新输入
			mv.addObject("name2", name);
			mv.addObject("password2",password);
			mv.addObject("yanzhengma2", yanzhengma);
			mv.setViewName("denglu");
			return mv;
		}
		
		//用户不存在
		if(ct_userBoolean==false)
		{
			mv.addObject("message","用户不存在");
			//将用户输入的值返回去，以免重新输入
			mv.addObject("name2", name);
			mv.addObject("password2",password);
			mv.addObject("yanzhengma2", yanzhengma);
			mv.setViewName("denglu");
			return mv;
		}
		
		//用户存在，并且验证码正确，进入主页
		if(ct_userBoolean==true&&yanzhengmaBoolean==true){
			logger.info("用户名密码都正确，验证码也正确，成功进入");
			mv.setViewName("shangchuan");
			return mv;
		}
		return mv;
	}

	/*
	 * @method index.jsp里注销用户
	 */
	@RequestMapping("index")
	public ModelAndView Index(ModelAndView mv,HttpSession session){
		session.removeAttribute("ct_user");
		session.removeAttribute("username");
		mv.setViewName("index");
		return mv;
	}
}
