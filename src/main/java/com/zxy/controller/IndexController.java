package com.zxy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class IndexController {

	@RequestMapping("/hello")
	public String show(Model model)
	{
		model.addAttribute("name","张三");
		return "hello";
	}
	
//	根据输入的地址自动跳转
	@RequestMapping("/{urlname}")
	public String tiaozhuan(@PathVariable String urlname)
	{
		return urlname;
	}
}
