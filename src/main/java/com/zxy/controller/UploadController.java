package com.zxy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zxy.file.SaveMultipartFile;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.util.UUID;

@Controller
public class UploadController {
    
    //存储地址
    public String UoladAddress="H:\\workspace\\workspace\\SpringBoot_Parent\\SpringBoot_CuoTiGuanLi2\\src\\main\\resources\\static\\upload";
    
    @RequestMapping(value = "/upload" ,method = RequestMethod.POST)
    @ResponseBody
    public String uploadFile(MultipartFile file2)
    {
        SaveMultipartFile save=new SaveMultipartFile();
        String filename2=save.SaveFile(file2);
        return filename2;
    }
    
   /* @RequestMapping(value = "/upload" ,method = RequestMethod.POST)
    @ResponseBody
    public String uploadFile(MultipartFile file2, HttpServletRequest request)
    {
        try {
        	//创建文件在服务器存放的位置 ,得到的是一个绝对路径
            //String dir=request.getServletContext().getRealPath("/upload");

            String dir=UoladAddress;

            File fileDir=new File(dir);//新建
          //如果不存在这个目录，新建一个
//          if(!fileDir.exists())
//          {
//              fileDir.mkdirs();

               //生成文件在服务器存放的名字
               //先得到后缀名
                String fileSuffix= file2.getOriginalFilename().substring(file2.getOriginalFilename().lastIndexOf("."));

                //生成唯一的文件名
                String fileName= UUID.randomUUID().toString()+fileSuffix;

                //新建文件（路径/文件名.后缀名）
                File files=new File(fileDir+"/"+fileName);

                //上传
                file2.transferTo(files);
//            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return "上传失败";
        }
        return  "上传成功";
    }*/

    @RequestMapping(value = "/upload/batch" ,method =RequestMethod.POST)
    @ResponseBody
    public String uploadFiles(MultipartFile[] file2,HttpServletRequest request)
    {
        try {
        	//创建文件在服务器存放的位置 ,得到的是一个绝对路径
            //String dir=request.getServletContext().getRealPath("/upload");

            String dir=UoladAddress;

            File fileDir=new File(dir);//新建
          //如果不存在这个目录，新建一个
//          if(!fileDir.exists())
//          {
//              fileDir.mkdirs();

                for(int i=0;i<file2.length;i++)
                {
                	//生成文件在服务器存放的名字
                    //先得到后缀名
                    String fileSuffix = file2[i].getOriginalFilename().substring(file2[i].getOriginalFilename().lastIndexOf("."));

                    //生成唯一的文件名
                    String fileName = UUID.randomUUID().toString() + fileSuffix;

                    //新建文件（路径/文件名.后缀名）
                    File files = new File(fileDir + "/" + fileName);

                    //上传
                    file2[i].transferTo(files);
                }
//            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return "上传失败";
        }
        return  "上传成功";
    }
}
