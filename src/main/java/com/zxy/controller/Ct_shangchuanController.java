package com.zxy.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.ibatis.javassist.expr.NewArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.zxy.entity.Ct_shangchuan;
import com.zxy.entity.Ct_user;
import com.zxy.file.SaveMultipartFile;
import com.zxy.service.Ct_shangchuanService;

/**
 * 
 * @author zxy
 * @time 2019年8月15日18:00:27
 */
@Controller
public class Ct_shangchuanController {
	@Autowired
	private Ct_shangchuanService ct_shangchuanService;
	@Autowired
	private SaveMultipartFile saveMultipartFile;
	
	private Logger logger=LoggerFactory.getLogger(this.getClass());
	/*
	 * @methd shangcuan.jsp里的上传文件功能
	 */
	@RequestMapping(value="/ShangchuanStudy",method = RequestMethod.POST)
	public ModelAndView ShangchuanStudy(ModelAndView mv,HttpSession session,
			String subject,String theme,String content,MultipartFile file1,MultipartFile file2){
		logger.info("传进来的subject:"+subject+","+"theme:"+theme+","+"content:"+content);
		logger.info("image_timu:"+file1.getOriginalFilename()+",image_daan:"+file2.getOriginalFilename());
		//得到这个登录人的id_user
		Ct_user ct_user=(Ct_user)session.getAttribute("ct_user");
		String id_user=ct_user.getId_user();
		logger.info("这个人编号为："+id_user);
		//保存两张图片，并留下文件名
		String image_timu=saveMultipartFile.SaveFile(file1);
		String image_daan=saveMultipartFile.SaveFile(file2);
		logger.info("两张图片名字分别为："+image_timu+","+image_daan);
		//得到系统时间
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		String date=df.format(new Date());
		
		//所有需要信息都得到，开始保存信息
		Ct_shangchuan ct_shangchuan=new Ct_shangchuan();
		ct_shangchuan.setContent(content);
		ct_shangchuan.setId_user(id_user);
		ct_shangchuan.setImage_daan(image_daan);
		ct_shangchuan.setImage_timu(image_timu);
		ct_shangchuan.setSubject(subject);
		ct_shangchuan.setTheme(theme);
		ct_shangchuan.setDate(date);
		ct_shangchuanService.insertCt_shangchuan(ct_shangchuan);
		
		//保存成功
		logger.info(ct_shangchuan.toString());
		logger.info("保存上传信息成功");
		mv.setViewName("shangchuan");
		return mv;
	}
	
	/*
	 * @method 点击这个页面显示所有上传数学学习记录
	 */
	@RequestMapping("/zhuye-shuxue")
	public ModelAndView Zhuyeshuxue(ModelAndView mv,HttpSession session)
	{
		String subject="数学";
		//获取用户id_user
		Ct_user ct_user=(Ct_user)session.getAttribute("ct_user");
		String id_user=ct_user.getId_user();
		List<Ct_shangchuan> list=ct_shangchuanService.seleCt_shangchuans(id_user, subject);
		for(Ct_shangchuan c:list)
		{
			logger.info(c.toString());
		}
		mv.addObject("clist", list);
		mv.setViewName("zhuye-shuxue");
		return mv;
	}
	
	/*
	 * @method 点击这个页面显示所有上传英语学习记录
	 */
	@RequestMapping("/zhuye-yingyu")
	public ModelAndView Zhuyeyingyu(ModelAndView mv,HttpSession session)
	{
		String subject="英语";
		//获取用户id_user
		Ct_user ct_user=(Ct_user)session.getAttribute("ct_user");
		String id_user=ct_user.getId_user();
		List<Ct_shangchuan> list=ct_shangchuanService.seleCt_shangchuans(id_user, subject);
		for(Ct_shangchuan c:list)
		{
			logger.info(c.toString());
		}
		mv.addObject("clist", list);
		mv.setViewName("zhuye-yingyu");
		return mv;
	}
	
	/*
	 * @method 点击这个页面显示所有上传政治学习记录
	 */
	@RequestMapping("/zhuye-zhengzhi")
	public ModelAndView Zhuyezhengzhi(ModelAndView mv,HttpSession session)
	{
		String subject="政治";
		//获取用户id_user
		Ct_user ct_user=(Ct_user)session.getAttribute("ct_user");
		String id_user=ct_user.getId_user();
		List<Ct_shangchuan> list=ct_shangchuanService.seleCt_shangchuans(id_user, subject);
		for(Ct_shangchuan c:list)
		{
			logger.info(c.toString());
		}
		mv.addObject("clist", list);
		mv.setViewName("zhuye-zhengzhi");
		return mv;
	}
	
	/*
	 * @method 点击这个页面显示所有上传专业课学习记录
	 */
	@RequestMapping("/zhuye-zhuanyeke")
	public ModelAndView Zhuyezhuanyeke(ModelAndView mv,HttpSession session)
	{
		String subject="专业课";
		//获取用户id_user
		Ct_user ct_user=(Ct_user)session.getAttribute("ct_user");
		String id_user=ct_user.getId_user();
		List<Ct_shangchuan> list=ct_shangchuanService.seleCt_shangchuans(id_user, subject);
		for(Ct_shangchuan c:list)
		{
			logger.info(c.toString());
		}
		mv.addObject("clist", list);
		mv.setViewName("zhuye-zhuanyeke");
		return mv;
	}
}
