package com.zxy.entity;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

/**
 * @author zxy
 * @time 2019年8月13日16:56:46
 */
public class Ct_user implements Serializable{
	
	private Integer id;//序号
	private String id_user;//用户的序号
	
	@Length(min=1,max=10,message="用户名在1-10位")
	private String name;//用户名
	
	@Length(min=3,max=12,message="密码在3-12位")
	private String password;//密码
	
	@NotBlank(message="邮箱不能为空")
	@javax.validation.constraints.Email(message="必须是合法的邮箱")
	private String email;//邮箱
	
	public Ct_user(){
		super();
	}

	public Ct_user(Integer id, String name, String password, String email,
			String id_user) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.email = email;
		this.id_user = id_user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId_user() {
		return id_user;
	}

	public void setId_user(String id_user) {
		this.id_user = id_user;
	}

	@Override
	public String toString() {
		return "Ct_user [id=" + id + ", name=" + name + ", password="
				+ password + ", email=" + email + ", id_user=" + id_user + "]";
	}
	
	
}
