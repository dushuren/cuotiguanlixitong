package com.zxy.entity;

import java.io.Serializable;

/**
 * 
 * @author zxy
 * @time 2019年8月14日23:37:45
 * @medod 就是上传文章的地方
 */

public class Ct_shangchuan implements Serializable {
	
	private Integer id;//序号
	private String id_user;//每个人的序号
	private String subject;//科目
	private String theme;//主题
	private String content;//详细内容
	private String image_timu;//题目图片
	private String image_daan;//答案图片
	private String date;//日期
	
	public Ct_shangchuan(){
		super();
	}

	public Ct_shangchuan(Integer id, String id_user, String subject,
			String theme, String content, String image_timu, String image_daan,
			String date) {
		super();
		this.id = id;
		this.id_user = id_user;
		this.subject = subject;
		this.theme = theme;
		this.content = content;
		this.image_timu = image_timu;
		this.image_daan = image_daan;
		this.date = date;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getId_user() {
		return id_user;
	}

	public void setId_user(String id_user) {
		this.id_user = id_user;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage_timu() {
		return image_timu;
	}

	public void setImage_timu(String image_timu) {
		this.image_timu = image_timu;
	}

	public String getImage_daan() {
		return image_daan;
	}

	public void setImage_daan(String image_daan) {
		this.image_daan = image_daan;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Ct_shangchuan [id=" + id + ", id_user=" + id_user
				+ ", subject=" + subject + ", theme=" + theme + ", content="
				+ content + ", image_timu=" + image_timu + ", image_daan="
				+ image_daan + ", date=" + date + "]";
	}

}
