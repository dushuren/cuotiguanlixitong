package com.zxy.mapper;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.zxy.entity.Ct_shangchuan;

/**
 * 
 * @author zxy
 * @time 2019年8月15日17:52:22
 */
@Repository
public interface Ct_shangchuanMapper {
	/*
	 * @param com.zxy.entity.Ct_shangchuan
	 * @return void
	 * @method 插入上传的类
	 */
	void insertCt_shangchuan(Ct_shangchuan ct_shangchuan);
	
	/*
	 * @param HashMap<String,Object>
	 * @return List<com.zxy.entity>Ct_shangchuan
	 * @method 查找上传的学习记录
	 */
	List<Ct_shangchuan> selectCt_shangchuans(HashMap<String, Object>params);
}
