package com.zxy.mapper;

import java.util.HashMap;

import org.springframework.stereotype.Repository;

import com.zxy.entity.Ct_user;

/**
 * 
 * @author zxy
 * @time 2019年8月13日17:07:33
 * @methd 放与用户相关的基础方法，如增删改查
 */
@Repository
public interface Ct_userMapper {
	/*
	 * @param HashMap<String,Object>
	 * @return com.zxy.ct_user
	 */
	Ct_user selectOneUser (HashMap<String, Object> params);
	
	/*
	 * @param com.zxy.ct_user
	 * @return void
	 */
	void insertCt_user(Ct_user ct_user);
}
