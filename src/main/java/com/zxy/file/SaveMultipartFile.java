package com.zxy.file;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class SaveMultipartFile {
	//存储地址
    public String UoladAddress="H:\\workspace\\workspace\\SpringBoot_Parent\\SpringBoot_CuoTiGuanLi2\\src\\main\\resources\\static\\upload";
    private Logger logger=LoggerFactory.getLogger(this.getClass());
    
    /*
     * @param MultipartFile file
     * @return String filename 
     * @method 传进来文件，返回去文件名
     */
    public String SaveFile(MultipartFile file){
    	logger.info("原始的文件名为："+file.getOriginalFilename());
    	
    	//获得系统当前时间
	    Date d = new Date();
	    //yyyy-MM-dd HH:mm:ss
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String now = df.format(d);
		logger.info("保存文件里，当前时间为："+now);
		
		//文件不为空，就执行存储文件，否则返回空字符串""
		if(!file.isEmpty()){
			//先得到上传的文件的名字，弄个新名字，再新建个文件保存
			//先得到后缀名
            String fileSuffix= file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            logger.info("上传的文件后缀名为："+fileSuffix);
           //生成唯一的文件名  （时间+随机数+后缀名）
            String fileName=now+ UUID.randomUUID().toString()+fileSuffix;
            
            try {
            	 //新建文件（路径+"/"+文件名）
                File files=new File(UoladAddress+"/"+fileName);
                //上传
                file.transferTo(files);
                logger.info("上传文件成功，文件名为："+fileName);
                return fileName;
			} catch (Exception e) {
				logger.error("ERROR,上传文件错误");
			}
           
		}else {
			logger.info("上传文件失败，文件名为空字符串");
			return "";
		}
		return "";
    	
    }
}
