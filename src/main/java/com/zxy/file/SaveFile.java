package com.zxy.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

public class SaveFile {
	  // 文件上传的地址
      String dizhi = "H:/jepluse/place/XueXiaoSheTuan/WebContent/shangchuan/";
      //服务器地址
	//String dizhi = "/home/pan/tomcat/webapps/XueXiaoSheTuan/shangchuan";
	
      public SaveFile()
      {
    	  
      }
      
      //专门弄了个存储文件的函数
  	  public String saveFile(MultipartFile file){
  		  
  
  		    //获得系统当前时间
		    Date d = new Date();
		    //yyyy-MM-dd HH:mm:ss
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String now = df.format(d);
  		  
  			System.out.println("开始执行存储文件");
  			// 如果文件不为空
  			// 不能用file!=null,这个我测了的，啥都不传，都有
  			if (!file.isEmpty()) 
  			{
  					
  				// 文件名字
  				String filename = file.getOriginalFilename();
  				String hecheng= now+"+"+UUID.randomUUID() + "+" + filename;
  				// 最终创建的文件为：地址+（时间+随机数+名字）
  				String dizhi_filename = dizhi + hecheng;

  				System.out.println("file.getName()为：" + file.getName());
  				System.out.println("file.getOriginalFilename()为："
  							+ file.getOriginalFilename());

  				// 这里输入流，输出流都先定义为空，否则容易出错
  				InputStream inputStream = null;
  				FileOutputStream fileOutputStream = null;
  				try {
  					// 将文件放到输入流上
  					inputStream = file.getInputStream();
  					// 新建输出流，输出到的位置
  					// 这里出错了，new File里指的是一个文件，而不是目录！
  					fileOutputStream = new FileOutputStream(
  							new File(dizhi_filename));
  					// 新建一个byte字节大小来存储
  					byte[] b = new byte[inputStream.available()];

  					// 读取数据流（目标，从0开始，到结束）
  					inputStream.read(b, 0, inputStream.available());
  					// 输出输入流（目标，从0开始，到结束）
  					fileOutputStream.write(b, 0, b.length);
  					} catch (IOException e) {
  						// TODO Auto-generated catch block
  						e.printStackTrace();
  					} finally {
  						System.out.println("finally");
  						// finally无论是否抛出异常都会执行

  						// 1如果输入流不为空，关闭输入流，如果输出流不为空，关闭输出流
  						// 2都不为空，就返回上传界面，弹出消息上传成功
  						// 3否则，就弹出消息上传不成功
  						// 这里最上面的条件就是文件不为空，只用判断12,3在下面的else判断
  						if (inputStream != null) {
  							try {
  								inputStream.close();
  							} catch (IOException e) {
  								// TODO Auto-generated catch block
  								e.printStackTrace();
  							}
  						}

  						if (fileOutputStream != null) {
  							try {
  								fileOutputStream.close();
  							} catch (IOException e) {
  								// TODO Auto-generated catch block
  								e.printStackTrace();
  							}
  						}

  						if (fileOutputStream != null && inputStream != null) {
//  							mv.setViewName("index");
//  							return mv;
  							System.out.println("输入流输出流都不为空，已经关闭");
  							return hecheng;
  						}

  					}

  				}
  			else {
  				// 这里属于文件为空的情况下
  				System.out.println("文件为空");
  				return "";
//  				mv.setViewName("uploadTest");
//  				return mv;
  			    }

//  			return mv;
  		return "";
  		 
  	 }
      
}
