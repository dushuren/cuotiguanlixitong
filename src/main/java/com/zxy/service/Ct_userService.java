package com.zxy.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zxy.entity.Ct_user;
import com.zxy.mapper.Ct_userMapper;
/**
 * 
 * @author zxy
 * @time 2019年8月13日17:09:43
 * @method 放错题的综合的方法，如注册，需要先判断是否已经注册过，再增加这条消息
 */
@Service
public class Ct_userService {
	@Autowired
	private Ct_userMapper ct_userMapper;
	
	/*
	 * @param String name 
	 * @param Sring password
	 * @return com.zxy.ct_user
	 */
	public Ct_user selectUserByNameAndPassword(
			String name,String password)
	{ 	
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("name", name);
		params.put("password", password);
		return ct_userMapper.selectOneUser(params);
	}
	
	/*
	 * @param com.zxy.entity.ct_user
	 * @return void
	 * @method 插入一个用户
	 */
	public void insertCt_user(Ct_user ct_user)
	{
		ct_userMapper.insertCt_user(ct_user);
	}
	
	/*
	 * @param String name
	 * @retun com.zxy.entity.ct_user
	 * @method 查找该用户名是否已经注册过
	 */
	public Ct_user selectCt_userByname(String name)
	{
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("name", name);
		Ct_user ct_user=ct_userMapper.selectOneUser(params);
		return ct_user;
	}
}
