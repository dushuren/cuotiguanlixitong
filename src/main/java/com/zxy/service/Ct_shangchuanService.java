package com.zxy.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zxy.entity.Ct_shangchuan;
import com.zxy.mapper.Ct_shangchuanMapper;

/**
 * 
 * @author zxy
 * @time 2019年8月15日17:54:46
 */

@Service
public class Ct_shangchuanService {
	@Autowired
	private Ct_shangchuanMapper ct_shangchuanMapper;
	
	/*
	 * @param Ct_shangchuan
	 * @return void
	 * @method 插入一个上传信息
	 */
	public void insertCt_shangchuan(Ct_shangchuan ct_shangchuan){
		ct_shangchuanMapper.insertCt_shangchuan(ct_shangchuan);
	}
	
	/*
	 * @param String id_user
	 * @param String subject
	 * @return List<com.zxy.entity.Ct_shangchuan>
	 * @method 根据id_user和subject查找到这个用户上传的这个科目的信息
	 */
	public List<Ct_shangchuan> seleCt_shangchuans(String id_user,String subject)
	{
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("id_user", id_user);
		params.put("subject", subject);
		params.put("desc", "1");//根据id倒序
		List<Ct_shangchuan> list=ct_shangchuanMapper.selectCt_shangchuans(params);
		return list;
	}
}
