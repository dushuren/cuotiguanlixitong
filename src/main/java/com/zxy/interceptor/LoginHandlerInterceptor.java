package com.zxy.interceptor;

/*
 * 老师说可能是拦截器有问题或者是
 * mv.setViewName("redirect:main");这段代码的版本问题
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import com.zxy.entity.Ct_user;


/*
 * 拦截器必须实现HandlerInterceptor
 */
public class LoginHandlerInterceptor implements HandlerInterceptor {
	private Logger logger=LoggerFactory.getLogger(this.getClass());
	// 不拦截的请求
	private static final String[] IGNORE_URI = {
		"/index","/zhuce"};

	/*
	 * 该方法将在整个请求完成执行后执行，主要作用是用于清理资源，
	 * 该方法也只能在当前Interceptor的preHandle方法的返回值为true时才会执行。
	 */

	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception exception)
			throws Exception {
		logger.info("AuthorizationInterceptor afterCompletion-->");
	}

	/*
	 * 该方法将在Controller的方法调用之后执行，方法中可对ModelAndView进行操作，
	 * 该方法也只能在当前Interceptor的preHandle方法的返回值为trues是才会执行。
	 */
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			org.springframework.web.servlet.ModelAndView mv) throws Exception {
		logger.info("AuthorizationInterceptor postHandle-->");
	}

	/*
	 * preHandle方法是进行处理器拦截用的，该方法将在Controller处理之前进行调用，
	 * 该方法的返回值为true拦截器才会继续往下执行，该方法的返回值为false的时候整个请求就结束了。
	 */
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		logger.info("AuthorizationInterceptor preHandle-->");
		// flag变量用于判断用户是否登录，默认为false
		boolean flag = false;
		// 获取请求的路径进行判断
		String servletPath = request.getServletPath();
		// 判断请求是否需要拦截
		for (String s : IGNORE_URI) {
			if (servletPath.contains(s)) {
				flag = true;
				break;
			}
		}
		logger.info("进入拦截器");
		// 拦截请求
		if (!flag) {
			// 1.获取session中的用户
			Ct_user ct_user = (Ct_user) request.getSession().getAttribute("ct_user");
			logger.info("登录的用户是:"+ct_user);
			// 2.判断用户是否已经登录
			if (ct_user == null ) {
				// 如果用户没有登录，则设置提示信息，跳转到登录页面
				logger.info("AuthorizationInterceptor 拦截请求：");
				//request.setAttribute("message", "请先登录再访问网站");
				request.getRequestDispatcher("index").forward(request,response);
			} else {
				// 如果用户已经登录，则验证通过，放行
				logger.info("AuthorizationInterceptor 放行请求：");
				flag = true;
			}
		}
		
		return flag;

	}

}
