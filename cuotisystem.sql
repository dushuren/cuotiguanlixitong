/*
SQLyog Professional v12.08 (64 bit)
MySQL - 5.5.54-log : Database - cuotisystem
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cuotisystem` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cuotisystem`;

/*Table structure for table `ct_shangchuan` */

DROP TABLE IF EXISTS `ct_shangchuan`;

CREATE TABLE `ct_shangchuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `id_user` varchar(100) DEFAULT NULL COMMENT '每个人的序号',
  `subject` varchar(20) DEFAULT NULL COMMENT '科目',
  `theme` varchar(20) DEFAULT NULL COMMENT '主题',
  `content` text COMMENT '详细内容',
  `image_timu` varchar(100) DEFAULT NULL COMMENT '题目图片',
  `image_daan` varchar(100) DEFAULT NULL COMMENT '答案图片',
  `date` varchar(20) DEFAULT NULL COMMENT '日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_user` */

DROP TABLE IF EXISTS `ct_user`;

CREATE TABLE `ct_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `id_user` varchar(100) DEFAULT NULL COMMENT '用户编号',
  `name` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  `email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `userName` varchar(32) NOT NULL,
  `passWord` varchar(50) NOT NULL,
  `realName` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
